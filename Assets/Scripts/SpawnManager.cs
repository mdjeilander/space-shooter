﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnManager : MonoBehaviour
{
    [SerializeField]
    private GameObject _enemyPrefab;
    [SerializeField]
    private GameObject[] _powerups;

    [SerializeField]
    private GameObject _enemyContainer;

    private bool _stopSpawning = false;


    private void Start()
    {
        StartCoroutine(SpawnEnemyRoutine());
        StartCoroutine(PowerUpRoutine());
    }

    IEnumerator SpawnEnemyRoutine()
    {
        while (_stopSpawning == false)
        {
            yield return null;
            Vector3 position = new Vector3(Random.Range(-8f, 8f), 8, 0);
            GameObject newEnemy = Instantiate(_enemyPrefab, position, Quaternion.identity);
            newEnemy.transform.parent = _enemyContainer.transform;

            yield return new WaitForSeconds(5f);

        }
    }


    IEnumerator PowerUpRoutine()
    {
        while(_stopSpawning == false)
        {
            yield return null;
            Vector3 position = new Vector3(Random.Range(-8f, 8f), 8, 0);
            int randomPowerUp = Random.Range(0, 3);
            Instantiate(_powerups[randomPowerUp], position, Quaternion.identity);

            yield return new WaitForSeconds(Random.Range(3, 8));
        }

    }

    public void OnPlayerDeath()
    {
        _stopSpawning = true;
    }
}
